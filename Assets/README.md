# System Runtime Caching

## What
* System.Runtime.Caching v4.6.0 [.NETStandard 2.0]

## Installation

```bash
"com.yenmoc.system-runtime-caching":"https://gitlab.com/yenmoc/system-runtime-caching"
or
npm publish --registry=http://localhost:4873
```

