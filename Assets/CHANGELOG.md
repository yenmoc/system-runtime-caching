# Changelog
All notable changes to this project will be documented in this file.

## [4.6.0] - 27-10-2019
### Added
	-add System.Runtime.Caching v4.6.0 [.NETStandard 2.0]

